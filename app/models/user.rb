class User < ApplicationRecord
    has_secure_password
    has_many :articles,foreign_key: 'user_id'
end
