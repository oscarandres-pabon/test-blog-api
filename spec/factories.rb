FactoryBot.define do  

    factory :api_key do
      token {"SomeRandomToken"}
    end

    factory :user do
        name {"Joe"}
        email {"joe@gmail.com"}
        password {"blah"}        
    end
    factory :article do
      title {"article 1"}
      content {"Lorem Ipsum Lorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem IpsumLorem Ipsum"}
      slug {"blah--"}
      user_id {1}
    end
  end