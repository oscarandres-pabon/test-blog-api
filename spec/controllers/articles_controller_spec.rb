require "rails_helper"

RSpec.describe Api::V1::ArticlesController, type: :controller do
    # Create test user
    let!(:user) { create :user}        
    # Request Token for authorization
    let(:token) {}
    let(:header) { { 'Authorization' => token_generator(user.id) } }
    # Create Article for test
    let!(:article) { create :article}

    # Get Articles Case
    context "GET #index" do
        it "returns a success response" do
            get :index      
            expect(response).to have_http_status(:success)
        end
    end

    # Get Article Case
    context "GET #show" do                
        it "returns a success response" do            
            get :show, params:{id: article.id}            
            expect(response).to have_http_status(:success)            
        end
    end

    # New Article cases
    context "New Article" do
        let(:new_user) {{ "article":{"title": "test 4", "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean justo tortor, vulputate ut erat id, cursus imperdiet erat. Suspendisse tristique felis a mauris mattis, ut rhoncus turpis ultricies. Etiam dignissim posuere mollis. Vestibulum tempus erat nisl, sit amet feugiat ex commodo ut.", "slug": "lorem-ipsum", "user_id": 1}}}
        it "returns a success response" do
            request.headers.merge!(header)
            get :create, params: new_user
            expect(response).to have_http_status(:success)
        end
        it "returns a not authorized response (401)" do            
            get :create, params: new_user
            expect(response).to have_http_status(401)
        end
    end

    # Edit Article cases
    context "Edit Article" do        
        let(:attr) do 
            { :title => "test 4", :content => " Aenean justo tortor, vulputate ut erat id, cursus imperdiet erat. Suspendisse tristique felis a mauris mattis, ut rhoncus turpis ultricies. Etiam dignissim posuere mollis. Vestibulum tempus erat nisl, sit amet feugiat ex commodo ut." }
        end
         
        it "returns a success response" do
            request.headers.merge!(header)
            get :update, params:{id: article.id, article: attr}
            expect(response).to have_http_status(:success)
        end
        it "returns a not Authorized (401)" do            
            get :update, params:{id: article.id, article: attr}
            expect(response).to have_http_status(401)
        end

        it "returns a consistency of data response" do
            prev_updated_at = article.updated_at
            request.headers.merge!(header)
            get :update, params:{id: article.id, article: attr}            
            article.reload
            expect(article.updated_at).should_not eql(prev_updated_at)
            expect(article.title).to eql('test 4')
        end
        
    end

    # Delete cases
    context "Delete article " do                
        it "Returns a not Authorized (401)" do            
            get :destroy, params:{id: article.id}
            expect(response).to have_http_status(401)
        end

        it "Returns a success response" do      
            request.headers.merge!(header)
            get :destroy, params:{id: article.id}
            expect(response).to have_http_status(204)
        end
        it "Returns was deleted" do      
            request.headers.merge!(header)
            get :destroy, params:{id: article.id}
            expect(Article.all).to be_empty                      
        end
    end



    
end