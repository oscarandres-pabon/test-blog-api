class AddNetworkIdToUsers < ActiveRecord::Migration[6.0]
 def up
    add_column :articles, :user_id, :integer
    add_index  :articles, :user_id    
  end

  def down
    remove_index :articles, :id
    remove_column :articles, :id
  end
end
